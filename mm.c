/*
 * Each block has a header, a payload, and a footer.
 * Headers and footers contains size, allocation bit, and a list_elem for the free list.
 * Both allocated and free blocks have the same structure.
 *
 * References to free blocks are stored within the segregated free list.
 * The free lists use a power of 2 scheme, so list n stores blocks from 2^n to 2^(n+1)-1 words.
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stddef.h>
#include <string.h>
#include <assert.h>
#include "mm_ts.c"
#include "mm.h"
#include "memlib.h"
#include "list.h"
#include <pthread.h>
#define WSIZE sizeof(void*)
#define DSIZE 2*sizeof(void*)
#define CHUNKSIZE (1<<10)
#define MIN_BLOCK_SIZE_WORDS 2*sizeof(void*)
#define NUM_LISTS sizeof(void*) * 5

#define MAX(x, y) ((x) > (y) ? (x) : (y))

team_t team = {
	"jishnumattinz",
	"Jishnu Renugopal",
	"jishnu",
	"Mattin Zargarpur",
	"mattinz"
};

struct boundaryTag
{
	int inUse:1;
	int size:31;
	struct list_elem elem;
};

struct block
{
	struct boundaryTag header;
	char payload[0];
};

//Static Function Declarations
static struct block*        coalesce(struct block* bp);
static struct block*        extendHeap(size_t words);
static struct block*        findFit(size_t asize);
static size_t               getBlockSize(struct block* blk);
static struct boundaryTag*  getFooter(struct block* blk);
static int                  getIndex(size_t size);
static struct boundaryTag*  getPrevFooter(struct block* blk);
static bool                 isBlockFree(struct block* blk);
static void                 markBlockFree(struct block* blk, int size);
static void                 markBlockUsed(struct block* blk, int size);
static struct block*        nextBlock(struct block* blk);
static void                 place(struct block* bp, size_t asize);
static struct block*        prevBlock(struct block* bp);
static void                 setHeaderAndFooter(struct block* blk, int size, int inUse);

#ifdef THREAD_SAFE
static pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
#endif

//Static Variables
static const struct boundaryTag HEAP_FENCE = {.inUse = 1, .size = 0};
static struct list freeLists[NUM_LISTS];
static struct block* heapList = 0;

/*
 * Initializes the memory allocator.
 */
extern int mm_init()
{
	int idx;
	for(idx = 0; idx < NUM_LISTS; idx++)
	{
		list_init(&freeLists[idx]);
	}


	//Create empty heap.
	struct boundaryTag* initialHeap = mem_sbrk(2 * sizeof(struct boundaryTag));
	if(initialHeap == (void*)-1)
	{
		return -1;
	}

	initialHeap[0] = HEAP_FENCE;
	heapList = (struct block*)&initialHeap[1];
	initialHeap[1] = HEAP_FENCE;

	if(extendHeap(CHUNKSIZE) == NULL)
	{
		return -1;
	}

	return 0;
}

/*
 * Allocates at least size bytes.
 * If successful, returns a pointer to the first address in the allocated space.
 * Otherwise, returns NULL.
 */
extern void* mm_malloc(size_t size)
{
	#ifdef THREAD_SAFE
	pthread_mutex_lock(&lock);
	#endif
	size_t awords;
	size_t extendwords;
	struct block* bp;

	if(heapList == 0)
	{
		mm_init();
	}

	if(size == 0)
	{
		#ifdef THREAD_SAFE
	        pthread_mutex_unlock(&lock);
        	#endif

		return NULL;
	}

	//Adjust block size to include overhead and meet alignment requirement.
	size += 2 * sizeof(struct boundaryTag);
	size = (size + DSIZE - 1) & ~(DSIZE - 1);
	assert(size % 8 == 0);
	awords = MAX(MIN_BLOCK_SIZE_WORDS, size/WSIZE);

	bp = findFit(awords);
	if(bp == NULL)
	{
		//Extend heap if no fit found.
		extendwords = MAX(awords, CHUNKSIZE);
		bp = extendHeap(extendwords);
		if(bp == NULL)
		{
			#ifdef THREAD_SAFE
        		pthread_mutex_unlock(&lock);
        		#endif

			return NULL;
		}
	}
	place(bp, awords);
	#ifdef THREAD_SAFE
        pthread_mutex_unlock(&lock);
        #endif

	return bp->payload;
}

/*
 * Frees the space pointed to by ptr.
 * ptr must be a pointer returned by a previous call to mm_malloc or mm_realloc.
 */
extern void mm_free(void* ptr)
{
	#ifdef THREAD_SAFE
        pthread_mutex_lock(&lock);
        #endif

	if(ptr == 0)
	{
		#ifdef THREAD_SAFE
        	pthread_mutex_unlock(&lock);
        	#endif

		return;
	}

	struct block* blk = ptr - offsetof(struct block, payload);
	if(isBlockFree(blk))
	{
		#ifdef THREAD_SAFE
        	pthread_mutex_unlock(&lock);
        	#endif

		return;
	}
	if(heapList == 0)
	{
		mm_init();
	}

	markBlockFree(blk, getBlockSize(blk));
	coalesce(blk);
	#ifdef THREAD_SAFE
        pthread_mutex_unlock(&lock);
        #endif

}

/*
 * Resizes a previous request for memory to be at least size bytes long.
 * ptr must be a pointer returned by a previous call to mm_malloc.
 * Returns a pointer to the resized space, which may not be the same as ptr.
 */
extern void* mm_realloc(void* ptr, size_t size)
{
	#ifdef THREAD_SAFE
        pthread_mutex_lock(&lock);
        #endif

	if(size == 0)
	{
		#ifdef THREAD_SAFE
        	pthread_mutex_unlock(&lock);
        	#endif

		mm_free(ptr);
		return NULL;
	}

	if(ptr == NULL)
	{
		#ifdef THREAD_SAFE
        	pthread_mutex_unlock(&lock);
        	#endif

		return mm_malloc(size);
	}


	size_t newSize = size + 2 * sizeof(struct boundaryTag);
	newSize = (newSize + DSIZE - 1) & ~(DSIZE - 1);
	size_t newWords = MAX(MIN_BLOCK_SIZE_WORDS, newSize/WSIZE);
	struct block* bp = ptr - offsetof(struct block, payload);
	if(newWords <= getBlockSize(bp))
	{
		#ifdef THREAD_SAFE
        	pthread_mutex_unlock(&lock);
        	#endif

		return ptr;
	}

	struct block* next = nextBlock(bp);
        if((isBlockFree(next) && newWords <= getBlockSize(bp) + getBlockSize(next)) || getBlockSize(next) == 0)
        {
		if(getBlockSize(next) == 0)
		{
			next = extendHeap(MAX(CHUNKSIZE, newWords));
		}

                list_remove(&next->header.elem);
                setHeaderAndFooter(bp, getBlockSize(bp) + getBlockSize(next), 1);
		#ifdef THREAD_SAFE
	        pthread_mutex_unlock(&lock);
        	#endif

                return ptr;

        }
	
	#ifdef THREAD_SAFE
        pthread_mutex_unlock(&lock);
        #endif

	void* newPtr = mm_malloc(size);
	if(newPtr == NULL)
	{
		return NULL;
	}
	size_t oldSize = getBlockSize(bp) * WSIZE;
	if(size < oldSize)
	{
		oldSize = size;
	}
	memcpy(newPtr, ptr, oldSize);
	mm_free(ptr);
	//#ifdef THREAD_SAFE
       // pthread_mutex_unlock(&lock);
        //#endif

	return newPtr;
}

/////////////////////////////
//STATIC FUNCTIONS
/////////////////////////////
/*
 * Combines the block pointed to by bp with any adjacent free blocks.
 * Returns a pointer to the coalesced block.
 */
static struct block* coalesce(struct block* bp)
{
	bool prevAllocated = getPrevFooter(bp)->inUse;
	bool nextAllocated = !isBlockFree(nextBlock(bp));
	size_t size = getBlockSize(bp);

	if(prevAllocated && nextAllocated)
	{
		return bp;
	}
	else if(prevAllocated && !nextAllocated)
	{
		struct block* next = nextBlock(bp);
                list_remove(&(next->header.elem));
		setHeaderAndFooter(bp, size + getBlockSize(next), 0);
	}
	else if(!prevAllocated && nextAllocated)
	{
		list_remove(&(bp->header.elem));
		bp = prevBlock(bp);
		setHeaderAndFooter(bp, size + getBlockSize(bp), 0);
	}
	else
	{
		struct block* next = nextBlock(bp);
		list_remove(&(bp->header.elem));
                list_remove(&(next->header.elem));
		setHeaderAndFooter(prevBlock(bp), size + getBlockSize(prevBlock(bp)) + getBlockSize(next), 0);
		bp = prevBlock(bp);
	}

	list_remove(&(bp->header.elem));
	int idx = getIndex(getBlockSize(bp));
	list_push_front(&(freeLists[idx]), &(bp->header.elem));

	return bp;
}

/*
 * Extends the heap by words number of words and returns a pointer to the new memory.
 */
static struct block* extendHeap(size_t words)
{
	void* bp;

	words = (words + 1) & ~1;
	assert(words % 2 == 0);
	if((long)(bp = mem_sbrk(words * WSIZE)) == -1)
	{
		return NULL;
	}

	struct block* blk = bp - sizeof(HEAP_FENCE);
	markBlockFree(blk, words);
	nextBlock(blk)->header = HEAP_FENCE;

	return coalesce(blk);
}

/*
 * Attempts to find a free block large enough to accomodate asize words.
 * If successful, returns a pointer to a block.
 * Otherwise, returns NULL.
 */
static struct block* findFit(size_t asize)
{
	struct list* freeList;
	int idx = getIndex(asize) + 1;
	while(idx < NUM_LISTS)
	{
		freeList = &(freeLists[idx]);
		if(!list_empty(freeList))
		{
			return (struct block*)list_entry(list_pop_front(freeList), struct boundaryTag, elem);
		}
		idx++;
	}
	return NULL;
}

/*
 * Returns the size of block *blk.
 */
static size_t getBlockSize(struct block* blk)
{
	return blk->header.size;
}

/*
 * Returns a pointer to the footer of *blk.
 */
static struct boundaryTag* getFooter(struct block* blk)
{
	return (void*)((size_t*)blk + blk->header.size) - sizeof(struct boundaryTag);
}

/*
 * Returns the index of the freelist which stores blocks of size words.
 */
static int getIndex(size_t size)
{
        int idx = 0;
        while(idx < NUM_LISTS - 1 && size > 1)
        {
                size >>= 1;
                idx++;
        }
        return idx;
}

/*
 * Returns a pointer to the footer of the block preceding *blk.
 */
static struct boundaryTag* getPrevFooter(struct block* blk)
{
	return &blk->header - 1;
}

/*
 * Returns true if *blk is free. False otherwise.
 */
static bool isBlockFree(struct block* blk)
{
	return !blk->header.inUse;
}

/*
 * Marks a block free and inserts it into the appropriate freelist.
 */
static void markBlockFree(struct block* blk, int size)
{
	setHeaderAndFooter(blk, size, 0);
	int idx = getIndex(getBlockSize(blk));
        list_push_front(&(freeLists[idx]), &(blk->header.elem));
}


/*
 * Marks a block used and removes it from its freelist.
 */
static void markBlockUsed(struct block* blk, int size)
{
        setHeaderAndFooter(blk, size, 1);
	list_remove(&(blk->header.elem));
}

/*
 * Returns a pointer to the block succeeding *blk.
 */
static struct block* nextBlock(struct block* blk)
{
	assert(getBlockSize(blk) != 0);
	return (struct block*)((size_t*)blk + blk->header.size);
}

/*
 * Set *bp as a used block of asize words and split it if possible.
 */
static void place(struct block* bp, size_t asize)
{
	size_t blockSize = getBlockSize(bp);
	if((blockSize - asize) >= MIN_BLOCK_SIZE_WORDS)
	{
		markBlockUsed(bp, asize);
		bp = nextBlock(bp);
		markBlockFree(bp, blockSize - asize);
	}
	else
	{
		markBlockUsed(bp, blockSize);
	}
}

/*
 * Returns a pointer to the block preceding *bp.
 */
static struct block* prevBlock(struct block* bp)
{
	struct boundaryTag* prevFooter = getPrevFooter(bp);
	assert(prevFooter->size != 0);
	return (struct block*)((size_t*)bp - prevFooter->size);
}

/*
 * Sets *blk as a block of size words and inUse status.
 */
static void setHeaderAndFooter(struct block* blk, int size, int inUse)
{
	assert(inUse == 0 || inUse == 1);
	blk->header.inUse = inUse;
	blk->header.size = size;
	*getFooter(blk) = blk->header;
}
